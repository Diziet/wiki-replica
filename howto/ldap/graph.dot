# © 2020 Antoine Beaupré <anarcat@debian.org>, CC-BY-SA 4.0
digraph ldap {
        # general settings
        label="LDAP server architecture, torproject.org, october 2020"
        labelloc=top

        # fonts
        graph [ fontname=Liberation fontsize=14 ];
        node [ fontname=Liberaion ];
        edge [ fontname=Liberation ];

        subgraph "clusterdb" {
                label = "db.torproject.org"
                LDAP [ shape=cylinder ]
                "master files" [ shape=box ]
                sshdist [ label="sshdist@sshd" ]
                udreplicate_db [ label="ud-replicate" ]
                udmailgate [ label="ud-mailgate" ]
                LDAP -> udmailgate [ dir=both ]
                postfix_ldap -> udmailgate
                postfix_ldap [ label="postfix" ]
                udmailgate -> keyring [ arrowhead=none label=uses]
                LDAP -> "ud-generate" -> "master files"
                "master files"  -> udreplicate_db -> { "local files", keyring, authkeys_sshdist }
                "master files" -> sshdist
                sshdist -> authkeys_sshdist [ arrowhead=none label=uses]
                userdir_ldap_cgi [ label="userdir-ldap-cgi" ]
                LDAP -> userdir_ldap_cgi [ dir=both ]
                apache -> userdir_ldap_cgi
                {
                        # those are all files
                        rank=same
                        keyring [ shape=box ]
                        "local files" [shape=box ]
                        authkeys_sshdist [ shape=box label="/etc/ssh/userkeys/sshdist" ]
                }
                {
                        # bring up sshdist to compress graph
                        rank=same
                        LDAP
                        sshdist
                }
        }
        subgraph "clusterexample" {
                label = "common to all hosts"
                udreplicate_example [ label="ud-replicate" ]
                userkeys [ shape=box label="SSH user keys" ]
                note [ label="deployed from tarball to /var/lib/misc/userkeys/%u" shape=note ]
                note -> userkeys [ arrowhead=none ]
                {
                        rank=same
                        known_hosts [ shape=box label="/etc/ssh/known_hosts" ]
                        files_example [ shape=box label="..."]
                }

                sshdist -> udreplicate_example [ label=rsync ]
                udreplicate_example -> { NSS, PAM, files_example, userkeys, known_hosts }
                sudo -> PAM [ label=uses ]
                sshd -> NSS [ label=uses ]
                NSS [ shape=cylinder ]
                PAM [ shape=cylinder ]
        }
        subgraph "clustergit" {
                label = "git.torproject.org"
                udreplicate_git [ label="ud-replicate" ]
                sshdist -> udreplicate_git [ label=rsync ]
                udreplicate_git -> { files_git, gitolite_authkeys }
                {
                        # files
                        rank=same
                        files_git [ shape=box label="..."]
                        gitolite_authkeys [ shape=box label="~git/.ssh/authorized_keys" ]
                }
                sshd_git [ label="sshd" ]
                gitolite -> { gitolite_authkeys, sshd_git }  [ arrowhead=none label=uses ]
                auto_dns_git [ shape="cylinder" label="dns/auto-dns.git" ]
                domains_git [ shape="cylinder" label="dns/domains.git" ]
                letsencrypt_domains_git [ shape="cylinder" label="letsencrypt-domains.git" ]
        }

        subgraph "clusterdns" {
                label = "DNS servers"
                udreplicate_dns [ label="ud-replicate" ]
                sshdist -> udreplicate_dns [ label=rsync ]
                udreplicate_dns -> { dns_sshfp, dns_zone }
                puppet_agent [ label="Puppet agent" ]
                puppet_agent -> puppet_includes
                {
                        # files
                        rank=same
                        dns_zone [ shape=box ]
                        dns_sshfp [ shape=box ]
                        domains [ shape=box ]
                        auto_dns [ shape=box ]
                        le_snippets [ shape=box ]
                        puppet_includes [ shape=box ]
                }
                domains_git -> domains [ label="git pull" ]
                auto_dns_git -> auto_dns [ label="git pull" ]
                letsencrypt_domains_git -> le_snippets [ label="push hook" ]
                { dns_zone, dns_sshfp, domains, auto_dns, le_snippets, puppet_includes } -> zone_files
                zone_files [ shape=cylinder label="BIND zone files" ]
        }
        subgraph "clusterpuppet" {
                label = "puppet.torproject.org"
                PuppetDB [ shape=cylinder ]
                puppetd [ label="Puppet server" ]
                puppetd -> PuppetDB [ dir=both ]
                LDAP -> puppetd [ label="host metadata" ]
                puppetd -> puppet_agent [ label="TLSA records" ]
                abridged [ label="(ud-replicate omitted\nfor brievety)" shape=note ]
        }
        subgraph "clustermail" {
                label = "eugeni.torproject.org"
                postfix_mail [ label="postfix" ]
                udreplicate_eugeni [ label="ud-replicate" ]
                sshdist -> udreplicate_eugeni [ label=rsync ]
                udreplicate_eugeni -> aliases
                aliases [ shape=box label="/etc/postfix/debian aliases file" ]
                postfix_mail -> aliases [ label=uses arrowhead=none ]
        }
}
