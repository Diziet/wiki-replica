[[_TOC_]]

There was no meeting this month, here's a short technical report.

# Important announcements

Those are important announcements you might have missed:

 * fpcentral will be [retired][] on October 20th 2021, you are
   encouraged to use EFF's [Cover Your Tracks][] or [TorZillaPrint][]
   instead, see [ticket 40009][] for background
 * if you have a precious wiki page from Trac that redirects to some
   place no one can edit, [we welcome proposals on proper places to
   redirect those pages][]
 * we have a reliable [procedure to rename Git branches][] (e.g. to
   rename from `master` to `main`) globally
 * we set up a new onionbalance server so we can provide also v3 onion
   (balanced) services for all of our static websites.  See 
   <https://onion.torproject.org/> /
   <http://xao2lxsmia2edq2n5zxg6uahx6xox2t7bfjw6b5vdzsxi7ezmqob6qid.onion/>
   for a full list

[procedure to rename Git branches]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/gitlab/#renaming-a-branch-globally
[we welcome proposals on proper places to redirect those pages]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40233
[ticket 40009]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40009
[TorZillaPrint]: https://arkenfox.github.io/TZP/
[Cover Your Tracks]: https://coveryourtracks.eff.org/,
[retired]: https://lists.torproject.org/pipermail/tor-project/2021-April/003091.html

# Metrics of the month

 * hosts in Puppet: 86, LDAP: 89, Prometheus exporters: 140
 * number of Apache servers monitored: 28, hits per second: 162
 * number of Nginx servers: 2, hits per second: 2, hit ratio: 0.86
 * number of self-hosted nameservers: 6, mail servers: 7
 * pending upgrades: 1, reboots: 0
 * average load: 0.61, memory available: 1.94 TiB/2.77 TiB, running processes: 565
 * bytes sent: 246.89 MB/s, received: 147.97 MB/s
 * [GitLab tickets][]: 132 tickets including...
   * open: 0
   * icebox: 114
   * backlog: 15
   * next: 2
   * doing: 1
   * (closed: 2296)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

# Ticket analysis

| date       | open | icebox | backlog | next | doing | closed | delta | sum  | new  | spill |
|------------|------|--------|---------|------|-------|--------|-------|------|------|-------|
| 2020-11-18 | 1    | 84     | 32      | 5    | 4     | 2119   | NA    | 2245 | NA   | NA    |
| 2020-12-02 | 0    | 92     | 20      | 9    | 8     | 2130   | 11    | 2259 | 14   | -3    |
| 2021-01-19 | 0    | 91     | 20      | 12   | 10    | 2165   | 35    | 2298 | 39   | -4    |
| 2021-02-02 | 0    | 96     | 18      | 10   | 7     | 2182   | 17    | 2313 | 15   | 2     |
| 2021-03-02 | 0    | 107    | 15      | 9    | 7     | 2213   | 31    | 2351 | 38   | -7    |
| 2021-04-07 | 0    | 106    | 22      | 7    | 4     | 2225   | 12    | 2364 | 13   | -1    |
| 2021-05-03 | 0    | 109    | 15      | 2    | 2     | 2266   | 41    | 2394 | 30   | 11    |
| 2021-06-02 | 0    | 114    | 14      | 2    | 1     | 2297   | 31    | 2428 | 34   | -3    |
|------------|------|--------|---------|------|-------|--------|-------|------|------|-------|
| mean       | 0.1  | 99.9   | 19.5    | 7.0  | 5.4   | NA     | 22.2  | NA   | 22.9 | -0.6  |
<!-- #+TBLFM: @I*1$9=vsum($2..$7)::$10=@0$-1-@-1$-1::$11=$8-$10::@2$10=NA::@2$11=NA::$8=@0$-1-@-1$-1::@2$8=NA::@>$2..$6=vmean(@I..@II);%.1fEN::@>$8=vmean(@I..@II);%.1fEN::@>$10..$11=vmean(@I..@II);%.1fEN -->
<!-- the above is an org-mode table and can be reculated by -->
<!-- uncommenting the above formula and hitting "C-c C-c" -->

Yes, the Icebox is still filling up. Hopefully this will get resolved
soon-ish.

Legend:

 * date: date of the report
 * open: untriaged tickets
 * icebox: tickets triaged in the "icebox" ("stalled")
 * backlog: triaged, planned work for the "next" iteration (e.g. "next
   month")
 * next: work to be done in the current iteration or "sprint"
   (e.g. currently a month, so "this month")
 * doing: work being done right now (generally during the day or
   week)
 * closed: completed work
 * delta: number of new closed tickets from last report
 * sum: total number of tickets
 * new: tickets created since the last report
 * spill: difference between "delta" and "new", whether we closed more
   or less tickets than were created
