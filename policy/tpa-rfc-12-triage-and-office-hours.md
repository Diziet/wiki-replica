---
title: TPA-RFC-12: Triage, office hours formalization
---

[[_TOC_]]

Summary: this RFC changes TPA-RFC-2 to formalize the triage and
office hours process, among other minor support policy changes.

# Background

Since we have migrated to GitLab (~June 2021), we have been using
GitLab dashboards as part of our ticket processing pipeline. The
triage system was somewhat discussed in [TPA-RFC-5: GitLab
migration][TPA-RFC-5] but it seems this policy could use more visibility or
clarification.

[TPA-RFC-5]: policy/tpa-rfc-5-gitlab

Also, since April 2021, TPA has been running an unofficial "office
hours", where we try to occupy a Big Blue Button room more or less
continuously during the day. Those have been hit and miss, in general,
but we believe it is worth formalizing this practice as well.

# Proposal

The proposal is to patch TPA-RFC-2 to formalize office hours as a
support channel but also document the triage process more clearly,
which includes changing the GitLab policy in TPA-RFC-5. 

It also clarifies when to use confidential issues.

## Scope

This affects the way TPA interacts with users and will, to a certain
extent, augment our workload. We should, however, consider that the
office hours (in particular) are offered on a "best-effort" basis and
might not be continually operated during the entire day.

## Actual changes

[Merge request 18][] adds "Office hours" and "Triage" section
to [TPA-RFC-2: support][TPA-RFC-2].  It also clarifies the ticket triage
process in [TPA-RFC-5][] along with confidential issues in TPA-RFC-2.

[Merge request 18]: https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/merge_requests/18
[TPA-RFC-2]: policy/tpa-rfc-2-support

# Deadline

This proposal will be adopted by 2021-09-21 unless an objection is
raised inside TPA.

# Status

This proposal is currently in the `obsolete` state.

# References

 * [TPA-RFC-2][] documents our support policies
 * [TPA-RFC-5][] documents the GitLab migration and ticket workflow
 * this book introduced the concept of an "interruption shield":
   Limoncelli, T. A., Hogan, C. J., Chalup, S. R. 2007. *The Practice
   of System and Network Administration*, 2nd edition. Addison-Wesley.
 * [tpo/tpa/team#40354][]: issue asking to clarify confidential issues
 * [tpo/tpa/team#40382][]: issue about triage process

[tpo/tpa/team#40382]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40382
[tpo/tpa/team#40354]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40354
