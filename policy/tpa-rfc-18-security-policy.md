---
title: TPA-RFC-18: Security Policy
---

[[_TOC_]]

Summary: this policy tries to address the [need for a general security
policy](https://gitlab.torproject.org/tpo/team/-/issues/41)

# Background

# Proposal

## Scope

## Affected users

## (etc)

# Examples

Examples:

 * ...

Counter examples:

 * ...

# Deadline

# Status

This proposal is currently in the `draft` state.

# References

Internal:

* [Legacy SecurityPolicy page](https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/SecurityPolicy)
* [TPA-RFC-7 - root access](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-7-root)
* [Establish a global disaster recovery plan (#40628) · Issues · The Tor Project / TPA / TPA team · GitLab](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40628)

External:

* [OpenSSL Security Policy](https://www.openssl.org/policies/secpolicy.html)
* [GitHub - lfit/itpol: Useful IT policies](https://github.com/lfit/itpol/)
