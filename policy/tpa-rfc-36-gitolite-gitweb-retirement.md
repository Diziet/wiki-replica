---
title: TPA-RFC-36: Gitolite, GitWeb retirement
---

[[_TOC_]]

Summary: TODO

# Background

TODO: background. why do we retire gitolite, how did we end up with gitlab

# Proposal

TODO: talk about what will happen to gitolite, gitweb
TODO: redirections, migration procedures
TODO: timeline
TODO: consider TPA's git infrastructure (puppet? anything else?)

## Requirements

TODO: requirements

### Must have

 * HTTPS-level redirections for `.git` URLs. For example,
   `https://git.torproject.org/tor.git` MUST redirect to
   `https://gitlab.torproject.org/tpo/core/tor.git`

### Nice to have

### Out of scope

# Personas

TODO: personas

Examples:

 * ...

Counter examples:

 * ...

# Alternatives considered

TODO: why can't we keep just a little gitolite? 
TODO: consider other forges?

# Costs

Staff.

TODO: donate more hardware to gitlab to compensate?

# Approval

TODO: TPA + tor-internal?

# Deadline

Requirements are open for comments until July 25th at the soonest or
mid-August at the latest.

TODO: define actual proposal deadline.

# Status

This proposal is currently in the `draft` state.

# References

This proposal is discussed in [issue tpo/tpa/team#40472](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40472).
