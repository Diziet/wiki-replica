Our Prometheus monitoring server is running out of space again. 6 months
ago, we bumped it to 80GB in the hope that it would be enough to cover
for a year of samples, but that turned out to be underestimated by about
25%, and we're going to run out of space in a month if we don't take
action.

I would like to propose to increase this by another 80GB, which would
cost 7EUR/mth. We have room in our discretionary budget for such an
eventuality.

This proposal is done in the spirit of our RFC policy:

https://gitlab.torproject.org/anarcat/wikitest/-/wikis/policy/tpa-rfc-1-policy/

# Deadline

Given that we will run out of space in 11 days if no action is taken, I
propose a 7 days deadline for this proposal, which I will enact next
tuesday if no one objects.

# Status

This proposal is currently in the `obsolete` state.

# References

See [policy/tpa-rfc-1-policy](policy/tpa-rfc-1-policy).
