all

# MD003 Header style
#
# default is "consistent", would be better to set to ATX (because it
# makes "outlines" and grepping more readable), but too many matches
exclude_rule "MD003"

# MD004 - Unordered list style
#
# we don't care if people switch halfway, for now, too many matches
exclude_rule "MD004"

# MD005 Inconsistent indentation for list items at the same level
#
# For some reason, it breaks with:
#
#  * a
#    * b
#    * c
#
# which looks fine to me
exclude_rule "MD005"

# MD006 starting bulleted lists at the beginning of the line
#
# too many matches
exclude_rule "MD006"

# MD007 Unordered list indentation
#
# same as the above
exclude_rule "MD007"

# MD009 trailing spaces
#
# no rationale provided by markdownlint, too many matches
exclude_rule "MD009"

# MD010 Hard tabs
#
# matches tabs even in code blocks, which is frustrating because
# sometimes tabs *are* used in terminal output
exclude_rule "MD010"

# MD013 - Line length
#
# no rationale provided by markdownlint, too many matches
exclude_rule "MD013"

# MD024 Multiple headers with the same content
#
# the rationale is that some markdown parser would generate duplicate
# headers anchors, but we consider those broken. it is not the case of
# the GitLab wiki parser in any case
exclude_rule "MD024"

# MD025 Multiple top level headers in the same document
#
# corollary of the above
exclude_rule "MD025"

# MD026 Trailing punctuation in header
#
# allow exclamation marks and question marks in headings
rule 'MD026', :punctuation => ".,;:"

# MD029 Ordered list item prefix
#
# allow 1. 1. 1. and 1.2.3....
exclude_rule "MD029"

# MD034 Bare URL used
#
# seems legit, but too many matches.
exclude_rule "MD034"

# MD033 Inline HTML
#
# <kbd> is too useful and pretty
exclude_rule "MD033"

# MD040 Fenced code blocks should have a language specified
#
# we have too many shell scripts and random samples
exclude_rule "MD040"

# MD041 First line in file should be a top level header
#
# it's fine to start a document without a heading. it's called a
# lead. the document title should be in the front matter, not in a
# heading
exclude_rule "MD041"

# MD046 Code block style
#
# no rationale provided, seems legit to mix ``` and prefixes
exclude_rule "MD046"
